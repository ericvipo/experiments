(function() {
	function getUsers(){
		return new Promise(function(resolve,reject){
			setTimeout( function() {
				console.log("Users are ready");
				resolve([1,2,3,4]);
			},800);
		});
	}

	function getProjects(){
		return new Promise(function(resolve,reject){
			setTimeout( function(){
				console.log("Projects are ready!");
				resolve();
			},400);
		});
	}

	function getIssues(){
		return new Promise(function(resolve,reject){
			setTimeout( function(){
				console.log("Issues are ready");
				resolve();
			},100);
		});
	}

  // getUsers();
  // getProjects();
  // getIssues();

  getUsers().then(getProjects).then(getIssues);

/* 	getUsers()
		.then(function(response){
			console.log(response);
			return getProjects();
		})
		.then(getIssues)
		.catch(function(error){
			console.error("ERROR!");
		}); */

})();
