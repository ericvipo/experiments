import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export const store = new Vuex.Store({
	state: {
		men: [],
		women: []
	},
	getters: {
		noMen: (state) => state.men.length,
		noWomen: (state) => state.women.length
	},
	mutations: {
		setMen: (state,men) => {state.men = men},
		setWomen: (state,women) => {state.women = women}
	},
/* 	actions: {
		loadMen({commit}){
			return axios.get('https://randomuser.me/api/?results=5000&gender=male').
				then((response) => {
          console.log(response.data.results);
          return response.data.results;
					return commit('setMen',response.data.results);
				});
		},
		loadWomen({commit}){
			return axios.get('https://randomuser.me/api/?results=1000&gender=female').
				then((response) => {
          console.log(response.data.results);          
          return response.data.results;
					return commit('setWomen',response.data.results);
				});
    },
    loadUsers({dispatch,commit}){
      return dispatch('loadMen').then(mens=>{
        dispatch('loadWomen').then(womans=>{
          commit("setMen",mens);
          commit("setWomen",womans)
        })
      })
    }
  } */
  actions: {
    async loadMen({commit}){
      let response = await axios.get('https://randomuser.me/api/?results=5000&gender=male');
      let men = await response.data.results;
      commit('setMen',men);
    },
    async loadWomen({commit}){
      let response = await axios.get('https://randomuser.me/api/?results=1000&gender=female');
      let women = await response.data.results;
      commit('setWomen',women);
    },
    async loadUsers({dispatch}){
      await dispatch('loadMen');
      await dispatch('loadWomen');
    }
  }
});
